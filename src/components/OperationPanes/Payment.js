import React from 'react';

import OptionsTablePair from '../OptionsTable/Pair';
import PubKeyPicker from '../FormComponents/PubKeyPicker.js';
import AmountPicker from '../FormComponents/AmountPicker.js';
import AssetPicker from '../FormComponents/AssetPicker.js';

export default function Payment(props) {

  let valueValidator = (value) => {
    let valueSize = new Buffer(value).length;
    if (valueSize > 64) {
      return `Entry value can only contain a maximum of 64 bytes. ${valueSize} bytes entered.`;
    }
  }

  return [
    <OptionsTablePair label="Destination" key="destination">
      <PubKeyPicker
        value={props.values['destination']}
        onUpdate={(value) => {props.onUpdate('destination', value)}}
        />
    </OptionsTablePair>,
    <OptionsTablePair label="Asset" key="asset">
      <AssetPicker
        value={props.values['asset']}
        onUpdate={(value) => {props.onUpdate('asset', value)}}
        />
    </OptionsTablePair>,
    <OptionsTablePair label="Amount" key="amount">
      <AmountPicker
        value={props.values['amount']}
        onUpdate={(value) => {props.onUpdate('amount', value)}}
        />
    </OptionsTablePair>,
    <OptionsTablePair label="Payload" key="payload">
      <TextPicker
        value={props.values['payload']}
        onUpdate={(value) => {props.onUpdate('payload', value)}}
        validator={nameValidator}
      />
    </OptionsTablePair>,
  ];
}
